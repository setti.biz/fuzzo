import time
from multiprocessing import Pool

import pandas as pd
from rapidfuzz.distance import JaroWinkler
from rapidfuzz.process_cpp import cdist

big_synthetic_df = pd.read_parquet('big_synthetic_df.parquet')

rules = [
    {"field": "name", type: "Exact", "weight": 50},
    {"field": "email", type: "Exact", "weight": 20},
    {"field": "address", type: "Fuzzy", "weight": 30}
]


def combined_similarity(x, y, score_cutoff=None):
    name_similarity = x[0] == y[0] * rules[0]['weight']
    email_similarity = x[1] == y[1] * rules[1]['weight']
    if (name_similarity + email_similarity + rules[2]['weight']) < score_cutoff:
        return 0

    # address_similarity = JaroWinkler.distance(x[2], y[2]) * rules[2]['weight']
    address_similarity = JaroWinkler.distance(x[2], y[2])
    return name_similarity + email_similarity + address_similarity


data = big_synthetic_df[['Address', 'Name', 'Email']].values

per_page = 10
total_pages = 20
pool_size = 16
score_cutoff = 80


def do_job(page):
    index_from = page * per_page
    index_to = index_from + per_page
    dist_mt = cdist(data[index_from:index_to], data[0:10000000], scorer=combined_similarity, score_cutoff=score_cutoff)
    print(f"Done {index_from} to {index_to} with shape {dist_mt.shape}")


if __name__ == '__main__':
    time_start = time.time()
    print(f"Starting with {pool_size} processes and {total_pages} pages of {per_page} rows each")
    with Pool(pool_size) as p:
        print(p.map(do_job, range(total_pages)))
    time_end = time.time()
    print(f"Time taken: {(time_end - time_start) / 60} minutes")
