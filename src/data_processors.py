import io
import os
import random
import string

import pandas as pd
from numpy.random import choice


def random_string(probability):
    r_string = ''.join(random.choice(string.ascii_lowercase) for i in range(5))
    return choice([r_string, ''], p=[probability, 1 - probability])


def random_string_series(n, probability):
    return pd.Series([random_string(probability) for i in range(n)])


def generate_synthetic_data(multiplier):
    s = """Name           Email                    Address         
    0    James           james@email.com          123 Park Ave
    1    Bike            bike@email.com           123 Park Ave
    2    James Marsh     james@email.com          123 Park Ave
    3    Job             james@email.com          456 Park Ave
    4    Michael         m1@email.com             789 Park Ave
    5    Mike K          k2@email.com             419 Park Ave
    6    Michael k       k2@email.com             467 Park Ave
    7    James           james2@email.com         124 Park Ave"""
    df = pd.read_csv(io.StringIO(s), sep='\s\s+', engine='python')
    df['Name'] = df['Name'].str.lower()
    df['Email'] = df['Email'].str.lower()

    _synthetic_df = pd.DataFrame(columns=df.columns)
    for i in range(multiplier):
        for _, row in df.iterrows():
            synthetic_row = row.copy()
            synthetic_row['Name'] += f"{i} "
            synthetic_row['Email'] = synthetic_row['Email'].replace('@', f'{i}@')
            synthetic_row['Address'] = synthetic_row['Address'] + f" Apt {i}"
            _synthetic_df = pd.concat([_synthetic_df, synthetic_row.to_frame().T], ignore_index=True)

    return _synthetic_df


def get_synthetic_data(multiplier, noise_factor=0.5):
    filename = f'data/big_synthetic_df_{multiplier}_noise_{noise_factor}.parquet'
    if filename not in os.listdir():
        synthetic_df = generate_synthetic_data(int(multiplier / 8))
        big_df = pd.concat([synthetic_df] * 100, ignore_index=True)
        big_df['Name'] = big_df['Name'] + random_string_series(big_df.shape[0], probability=noise_factor)
        big_df['Email'] = big_df['Email'] + random_string_series(big_df.shape[0], probability=noise_factor)
        big_df.to_parquet(filename)

    return pd.read_parquet(filename)
